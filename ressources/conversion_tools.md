Conversion tools
=================

* * *

BAM to FASTQ
------------

This section gives a description of the command job you need to use if you want to convert the new MUGQIC raw data format (BAM) into the older format (FASTQ).

 

The simplest way to do it, is to process the BAM files using the **PICARD** tools fonction:[SamToFastq](http://broadinstitute.github.io/picard/command-line-overview.html#SamToFastq).

PICARD requires that Java is installed in your computer and accessible in your $PATH variable environment.  



The following command corresponds to the conversion of a paired-end BAM into 2 fastq files (pair 1 and pair 2)
```
java -jar <PATH_TO_PICARD_JAR_FOLDER>/SamToFastq.jar VALIDATION_STRINGENCY=SILENT INPUT=<INPUT_BAM_FILE_PATH> FASTQ=<OUTPUT_FASTQ_PAIR1_FILE_PATH> SECOND_END_FASTQ=<OUTPUT_FASTQ_PAIR2_FILE_PATH>`  
```





Note that additional java options could be used in order to improve the performance of the conversion and to fit the resources of the computer or cluster the process is run on:

|java option| Description|
|:----------|:-----------| 
|-Djava.io.tmpdir=string |path to the temporary directory for internal IO|
|XX:ParallelGCThreads=number|number of thread available for java|
|-Xmx<N>|<N> = amount of RAM allocated for java (i.e -Xmx15G)|

To avoid introducing bias in some order-sensitive downstream analyses, some users may also consider shuffling or resorting the input bam or output fastq. Sorting the bam by query name  also has the benefit of speeding up SamToFastq.


For applications such as Hi-C, paired reads may systematically align far apart, resulting in prohibitive SamToFastq memory consumption. In such cases, it can be beneficial to sort BAM files by queryname prior to running SamToFastq:
```
java -jar <PATH_TO_PICARD_JAR_FOLDER>/SortSam.jar VALIDATION_STRINGENCY=SILENT INPUT=<INPUT_BAM_FILE_PATH> OUTPUT=<OUTPUT_BAM_FILE_PATH> SORT_ORDER=queryname
```


* * *

gff3 to gtf2
------------

Use the Cufflinks **gffread** utility. The program gffread is included with the Cufflinks package and it can be used to verify or perform various operations on GFF files (use gffread -h to see the various usage options). Because the program shares the same GFF parser code with Cufflinks and other programs in the Tuxedo suite, it could be used to verify that a GFF file from a certain annotation source is correctly "understood" by these programs. Thus the gffread utility can be used to simply read the transcripts from the file and print these transcripts back, in either GFF3 (default) or GTF2 format (-T option), discarding any extra attributes and keeping only the essential ones, so the user can quickly verify if the transcripts in that file are properly parsed by the GFF reader code. The command line for such a quick cleanup and visual inspection of a given GFF file could be:

Example:

` 
gffread -E annotation.gff -T -o- > annotation.gff
`


This will reformat the transcript records given in the input file (annotation.gff in this example) from GFF3 to GTF2. The -E option directs gffread to "expose" (display warnings about) any potential issues encountered while parsing the given GFF file.

[Click here for more information](http://cole-trapnell-lab.github.io/cufflinks/file_formats/#gff3).