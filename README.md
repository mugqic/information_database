MUGQIC -- information database
================

Information database on Next Generation Sequencing (NGS) data analysis 

For comments and question please send an email to bioinformatics.service@mail.mcgill.ca


## available information ressources:

* [Conversion tools](ressources/conversion_tools.md)
* [Method descriptions](ressources/method_descriptions.md)
